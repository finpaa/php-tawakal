<?php

namespace Finpaa\Sweden;

use Finpaa\Finpaa;

class Tawakal
{
    private static $products;
    private static $ACCESS_TOKEN;

    private static function selfConstruct()
    {
        $finpaa = new Finpaa();
        self::$products = $finpaa->Sweden()->{env('FINPAA_ENVIRONMENT')}()->Tawakal()->Products()->default();
    }

    private static function getSequenceCode($name = 'products')
    {
        if(self::$$name) {
            return self::$$name;
        }
        else {
            self::selfConstruct();
            return self::getSequenceCode($name);
        }
    }

    private static function executeMethod($methodCode, $alterations, $returnPayload = false)
    {
        $response = Finpaa::executeTheMethod($methodCode, $alterations, $returnPayload);
        return array('error' => false, 'response' => json_decode(json_encode($response), true));
    }

    static function getAccessToken()
    {
        if(self::$ACCESS_TOKEN)
        {
            return self::$ACCESS_TOKEN;
        }
        else
        {
            $alterations[]['headers']['Authorization'] = "Basic " . base64_encode(env("ROARING_CLIENT_ID") . ":" . env("ROARING_CLIENT_SECRET"));
            $response = self::login();

            if(!$response['error'] && isset($response['response']['token']))
                self::$ACCESS_TOKEN = $response['response']['token'];
            else self::$ACCESS_TOKEN = "Unauthorized";
            return self::getAccessToken();
        }
    }

    static function getCountries()
    {
        $methodCode = self::getSequenceCode()->getCountry();

        $alterations[]['headers'] = array(
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        );

        $alterations[]['body'] = array(
            'username' => env('TAWAKAL_USERNAME'),
            'password' => env('TAWAKAL_PASSWORD'),
            'agentcode' => env('TAWAKAL_AGENT_CODE')
        );

        $response = self::executeMethod($methodCode, $alterations);

        return $response;
    }

    static function getCustomerIDType($countryCode)
    {
        $methodCode = self::getSequenceCode()->getCustomerIDType();

        $alterations[]['headers'] = array(
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        );

        $alterations[]['body'] = array(
            'username' => env('TAWAKAL_USERNAME'),
            'password' => env('TAWAKAL_PASSWORD'),
            'agentcode' => env('TAWAKAL_AGENT_CODE'),
            'concode' => $countryCode
        );

        $response = self::executeMethod($methodCode, $alterations);

        return $response;
    }

    static function getPurpose()
    {
        $methodCode = self::getSequenceCode()->getPurpose();

        $alterations[]['headers'] = array(
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        );

        $alterations[]['body'] = array(
            'username' => env('TAWAKAL_USERNAME'),
            'password' => env('TAWAKAL_PASSWORD'),
            'agentcode' => env('TAWAKAL_AGENT_CODE')
        );

        $response = self::executeMethod($methodCode, $alterations);

        return $response;
    }

    static function getSource()
    {
        $methodCode = self::getSequenceCode()->getSource();

        $alterations[]['headers'] = array(
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        );

        $alterations[]['body'] = array(
            'username' => env('TAWAKAL_USERNAME'),
            'password' => env('TAWAKAL_PASSWORD'),
            'agentcode' => env('TAWAKAL_AGENT_CODE')
        );

        $response = self::executeMethod($methodCode, $alterations);

        return $response;
    }

    static function getService($countryCode)
    {
        $methodCode = self::getSequenceCode()->getService();

        $alterations[]['headers'] = array(
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        );

        $alterations[]['body'] = array(
            'username' => env('TAWAKAL_USERNAME'),
            'password' => env('TAWAKAL_PASSWORD'),
            'agentcode' => env('TAWAKAL_AGENT_CODE'),
            'concode' => $countryCode
        );

        $response = self::executeMethod($methodCode, $alterations);

        return $response;
    }

    static function getAgentLocation($countryCode, $serviceCode)
    {
        $methodCode = self::getSequenceCode()->getAgentLocation();

        $alterations[]['headers'] = array(
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        );

        $alterations[]['body'] = array(
            'username' => env('TAWAKAL_USERNAME'),
            'password' => env('TAWAKAL_PASSWORD'),
            'agentcode' => env('TAWAKAL_AGENT_CODE'),
            'concode' => $countryCode,
            'servicecode' => $serviceCode
        );

        $response = self::executeMethod($methodCode, $alterations);

        return $response;
    }

    static function getCommission($body)
    {
        $methodCode = self::getSequenceCode()->getCommission();

        $alterations[]['headers'] = array(
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        );

        $alterations[]['body'] = array(
            'username' => env('TAWAKAL_USERNAME'),
            'password' => env('TAWAKAL_PASSWORD'),
            'agentcode' => env('TAWAKAL_AGENT_CODE'),
            'SendingLocationCode' => env('TAWAKAL_SENDING_LOCATION_CODE'),
        ) + $body;

        $response = self::executeMethod($methodCode, $alterations);

        return $response;
    }

    static function createTxn($body)
    {
        $methodCode = self::getSequenceCode()->createTxn();

        $alterations[]['headers'] = array(
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        );

        $alterations[]['body'] = array(
            'username' => env('TAWAKAL_USERNAME'),
            'password' => env('TAWAKAL_PASSWORD'),
            'agentcode' => env('TAWAKAL_AGENT_CODE'),
            'remittancelist' => $body
        );

        $response = self::executeMethod($methodCode, $alterations);

        return $response;
    }

    static function transactionQuery($referenceNo)
    {
        $methodCode = self::getSequenceCode()->transactionQuery();

        $alterations[]['headers'] = array(
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        );

        $alterations[]['body'] = array(
            'username' => env('TAWAKAL_USERNAME'),
            'password' => env('TAWAKAL_PASSWORD'),
            'agentcode' => env('TAWAKAL_AGENT_CODE'),
            'referenceno' => $referenceNo
        );

        $response = self::executeMethod($methodCode, $alterations);

        return $response;
    }

    static function checkStatus($referenceNo)
    {
        $methodCode = self::getSequenceCode()->checkStatus();

        $alterations[]['headers'] = array(
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        );

        $alterations[]['body'] = array(
            'username' => env('TAWAKAL_USERNAME'),
            'password' => env('TAWAKAL_PASSWORD'),
            'agentcode' => env('TAWAKAL_AGENT_CODE'),
            'referenceno' => $referenceNo
        );

        $response = self::executeMethod($methodCode, $alterations);

        return $response;
    }

    static function cancelTransaction($referenceNo)
    {
        $methodCode = self::getSequenceCode()->cancelTransaction();

        $alterations[]['headers'] = array(
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        );

        $alterations[]['body'] = array(
            'username' => env('TAWAKAL_USERNAME'),
            'password' => env('TAWAKAL_PASSWORD'),
            'agentcode' => env('TAWAKAL_AGENT_CODE'),
            'referenceno' => $referenceNo
        );

        $response = self::executeMethod($methodCode, $alterations);

        return $response;
    }
}
